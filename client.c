#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int handle_socket_output(int socket, char *buffer, int len){
	int	rc;			// Function outputs
	char* wrt_buffer = malloc(2*sizeof(char));


	if(len > 510){
		printf("Message is too big.\n");
		return -1;
	}
	if(len >= 255){
		wrt_buffer[0] = (char)255;
		wrt_buffer[1] = (char)(len-255);
	}
	else{
		wrt_buffer[0] = (char)0;
		wrt_buffer[1] = (char)len;
	}


	

	// Try to write first two bytes
	if((rc = send(socket, buffer, 2, 0)) == -1){
		perror("send to client failed");
		return -1;
	} else if(rc == 0) {
		printf("Client disconnected.\n");
		return -1;
	}


	// Read from socket
	if((rc = send(socket, buffer, len, 0)) == -1){
		perror("Send to client failed");
		return -1;
	} else if(rc == 0){
		printf("Client disconnected.\n");
		return -1;
	}

	// Returns 0 when OK
	return 0;
}
int handle_socket_input(int socket, char **buffer){
	int	rc;			// Function outputs
	int	read_data;	// Amount of data to read


	// Set up buffer to read first two bytes
	(*buffer) = realloc((*buffer), 2*sizeof(char));

	// Try to read first two bytes
	if((rc = recv(socket, (*buffer), 2, 0)) == -1){
		perror("read from client failed");
		return -1;
	} else if(rc == 0) {
		printf("Client disconnected.\n");
		return -1;
	}

	// Sum two bytes to get total amount of data to read
	read_data = ((int)(*buffer)[0]) + ((int)(*buffer)[1]);
	if(read_data == 0){
		// if not data to read, say goodbye
		(*buffer)[0] = 0;
		(*buffer)[1] = 3;
		send(socket,(*buffer),2, 0);
		send(socket,"bye", 3, 0);
		return -1;
	}

	// Read from socket
	(*buffer) = realloc((*buffer), read_data*sizeof(char));
	if((rc=recv(socket, (*buffer), read_data, 0)) == -1){
		perror("Read from client failed");
		return -1;
	} else if(rc == 0){
		printf("Client disconnected.\n");
		return -1;
	}

	return read_data;
}

int main(int argc, char const *argv[]){
	struct sockaddr_un addr;
	int socket_fd, rc;
	char *buffer = NULL;
	int socket_len;

	// socket for coms
	socket_fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if(socket_fd == -1){
		perror("socket() failed");
		exit(-1);
	}

	// set up addrs for connection
	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	memcpy(&addr.sun_path, "\0SOsocket", 9);
	// conenct to socket
	rc = connect(socket_fd, (struct sockaddr*)&addr, sizeof(addr));
	if(rc == -1){
		perror("connect() failed");
		close(socket_fd);
		exit(-1);
	}

	// mete aqui !!!!!!!!!!!!!!!!

	// asdasdasda
	
	// try to get info from db
	handle_socket_output(socket_fd, "5", 1);

	// inputs socket
	socket_len = handle_socket_input(socket_fd, &buffer);
	buffer = realloc(buffer, (socket_len + 1)*sizeof(char));
	buffer[socket_len] = 0;
	
	printf("Data read: %s \n", buffer);

	
	close(socket_fd);
	return 0;
}
