// compile with: gcc -Wall -Werror -Wno-strict-aliasing -pedantic-errors -std=gnu99 -g -ggdb3 -O2 -fomit-frame-pointer -pthread -o main main.c
//./main 4
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <strings.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

int send_fd(int sock, int fd){
	// This function does the arcane magic for sending
	// file descriptors over unix domain sockets
	char		buf[1];							// Temp buffer for iov
	struct		iovec iov[1];					// iov
	struct		msghdr msg;						// Message header for socket control
	struct		cmsghdr *cmsg ;					// Entry on message header
	char		cms[CMSG_SPACE(sizeof(int))];	// Control data

	// Set up iov with temp data
	iov[0].iov_base = buf;
	iov[0].iov_len = sizeof(buf);


	memset(&msg, 0, sizeof(struct msghdr));
	msg.msg_name = 0;
	msg.msg_namelen = 0;
	msg.msg_iov = iov;
	msg.msg_iovlen = 1;

	// Set msg to zero since we just want to send a fd
	memset(cms, 0, CMSG_SPACE(sizeof(int)));
	msg.msg_control = cms;
	msg.msg_controllen =  CMSG_SPACE(sizeof(int));

	// Set first header 
	cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type = SCM_RIGHTS;
	cmsg->cmsg_len = CMSG_LEN(sizeof(int));

	// Set file descripor to send over socket
	*((int *) CMSG_DATA(cmsg)) = fd;

	// Try to send fd
	return sendmsg(sock, &msg, 0);
}
int recv_fd(int socket) {
	// This function does the arcane magic for recving
	// file descriptors over unix domain sockets
	int 	rc;								// Function	outputs
	int 	fd;								// File descriptor to recv
	char 	buf[1];							// Temp buffer for iov
	struct 	iovec iov;						// iov
	struct 	msghdr msg;						// Message header for socket control
	struct 	cmsghdr *cmsg;					// Entry on message header
	char 	cms[CMSG_SPACE(sizeof(int))];	// Control data

	// Set up iov with temp data
	iov.iov_base = buf;
	iov.iov_len = 1;

	// Set msg to zero since we just want to recv a fd
	memset(&msg, 0, sizeof msg);
	msg.msg_name = 0;
	msg.msg_namelen = 0;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	// Set up control
	msg.msg_control = (caddr_t)cms;
	msg.msg_controllen = sizeof cms;

	// Try to receive msg
	if((rc=recvmsg(socket, &msg, 0)) == -1){
		perror("Failed to recv fd");
		return -1;
	}
	if(rc == 0){
		printf("Client closed while recv fd");
		return -1;
	}

	// Get first header from message control
	cmsg = CMSG_FIRSTHDR(&msg);
	// Copy fd data over from header
	memmove(&fd, CMSG_DATA(cmsg), sizeof(int));

	return fd;
}

int handle_socket_output(int socket, char *buffer, int len){
	int	rc;			// Function outputs
	char* wrt_buffer = malloc(2*sizeof(char));


	if(len > 510){
		printf("Message is too big.\n");
		return -1;
	}
	if(len >= 255){
		wrt_buffer[0] = (char)255;
		wrt_buffer[1] = (char)(len-255);
	}
	else{
		wrt_buffer[0] = (char)0;
		wrt_buffer[1] = (char)len;
	}


	

	// Try to write first two bytes
	if((rc = send(socket, buffer, 2, 0)) == -1){
		perror("send to client failed");
		return -1;
	} else if(rc == 0) {
		printf("Client disconnected.\n");
		return -1;
	}


	// Read from socket
	if((rc = send(socket, buffer, len, 0)) == -1){
		perror("Send to client failed");
		return -1;
	} else if(rc == 0){
		printf("Client disconnected.\n");
		return -1;
	}

	// Returns 0 when OK
	return 0;
}

int handle_socket_input(int socket, char **buffer){
	int	rc;			// Function outputs
	int	read_data;	// Amount of data to read


	// Set up buffer to read first two bytes
	(*buffer) = realloc((*buffer), 2*sizeof(char));

	// Try to read first two bytes
	if((rc = recv(socket, (*buffer), 2, 0)) == -1){
		perror("read from client failed");
		return -1;
	} else if(rc == 0) {
		printf("Client disconnected.\n");
		return -1;
	}

	// Sum two bytes to get total amount of data to read
	read_data = ((int)(*buffer)[0]) + ((int)(*buffer)[1]);
	if(read_data == 0){
		// if not data to read, say goodbye
		(*buffer)[0] = 0;
		(*buffer)[1] = 3;
		send(socket,(*buffer),2, 0);
		send(socket,"bye", 3, 0);
		return -1;
	}

	// Read from socket
	(*buffer) = realloc((*buffer), read_data*sizeof(char));
	if((rc=recv(socket, (*buffer), read_data, 0)) == -1){
		perror("Read from client failed");
		return -1;
	} else if(rc == 0){
		printf("Client disconnected.\n");
		return -1;
	}

	return read_data;
}


int **multiply_matrix(int **A, int **B, int Arow, int Acol, int Brow, int Bcol){
	if(Acol != Brow)
		return NULL;

	int ROW = Arow;
	int INNER = Acol;
	int COL = Bcol;

	int **C = malloc(ROW*sizeof(int*));

	for (int row = 0; row != ROW; ++row) {
		C[row] = malloc(COL*sizeof(int));

 		for (int col = 0; col != COL; ++col){
			int sum = 0;
			for (int inner = 0; inner != INNER; ++inner){
				sum += A[row][inner] * B[inner][col];
			}
			C[row][col] = sum;
		}
	}

	return C;
}

void *worker(void *mysocket_v){
	int		mysocket = (*((int**)mysocket_v))[0];		// listner->worker socket
	int		id = (*((int**)mysocket_v))[1];				// thread id
	int		db_fd = (*((int**)mysocket_v))[2];			// thread id
	int		client_socket;								// inbound connection socket
	int		read_len = 2;								// amount of data to read from socket
	char	*buffer = malloc(read_len*sizeof(char));	// currently read data
	int 	n_chars;

	// start worker routine
	while(1){
		printf("Thread %d waiting for work.\n", id);

		// read lates connection from listner->worker queue
		if((client_socket = recv_fd(mysocket)) == -1){
			perror("failed thread recv_fd()");
			return (void *) -1;
		}

		// get input from connected socket
		if((read_len = handle_socket_input(client_socket, &buffer)) == -1){
			close(client_socket);
			continue;
		}
		buffer = realloc(buffer,(read_len+1)*sizeof(char));
		buffer[read_len] = 0;

		// convert read data to int
		n_chars = atoi(buffer);
		// move file pointer to the beginning of the file 
		lseek(db_fd, 0, SEEK_SET);

		// read n_char chars from db
		buffer = realloc(buffer, n_chars*sizeof(char));
		n_chars = read(db_fd, buffer, n_chars);

		handle_socket_output(client_socket, buffer, n_chars);

		printf("Thread %d sent \"%s\"\n", id, buffer);



		// close socket connection once done
		close(client_socket);
	}

	return (void *)0;
}

int main(int argc, char **argv){
	struct 		sockaddr_un addr;	// socket address
	int 		sock;				// actual socket listner/connector
	int			conn;				// received connection
	int			pair_sd[2];			// socket pair for listner->workers coms
	int			thread_count = 0;	// numer of threads to run
	pthread_t	*worker_t;			// worker threads
	int			**thread_id;		// worker threads info
	int			i;
	int 		db_fd;


	// open/create db file
	db_fd = open("db.txt", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);

	// Create a unix domain socket for listnening incoming connections
	sock = socket(AF_UNIX, SOCK_STREAM, 0);
	if(sock == -1){
		perror("Failed to create socket listner");
		exit(-1);
	}

	// Bind the socket to it's "local" address
	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	memcpy(&addr.sun_path, "\0SOsocket",9);
	if(bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1){
		perror("Failed to bind to local address");
		exit(-1);
	}


	// Start listening
	if(listen(sock, 5) == -1){
		perror("Failed while starting to listen");
		exit(-1);
	}

	// Set up socket pair for listner->worker coms
	if(socketpair(AF_UNIX, SOCK_STREAM, 0, pair_sd) == -1){
		perror("Failed to create socketpair");
		exit(-1);
	}


	// Create workers
	if(argc > 1){
		// Read numer of threads to use from first argument
		thread_count = atoi(argv[1]);
	}
	// If invalid or no argument was provided, use default value of 4
	thread_count = (thread_count == 0)? 4 : thread_count;
	thread_id = malloc(thread_count*sizeof(int*));
	worker_t = malloc(thread_count*sizeof(pthread_t));
	for(i = 0; i < thread_count; i++){
		thread_id[i] = malloc(3*sizeof(int));
		thread_id[i][0] = pair_sd[1];
		thread_id[i][1] = i;
		thread_id[i][2] = db_fd;
		pthread_create(worker_t + i, NULL, worker, (void*)(thread_id + i));
	}


	// Start lister routine
	while(1){
		// Accept inbound connections to scoket
		printf("Waiting for connections.\n");
		conn = accept(sock, NULL, NULL);
		printf("Connection added to worker queue.\n");

		// Send connection socket fd to worker socket
		// We won't know which worker will get the socket
		// We just know one, and only one, will get it
		if(send_fd(pair_sd[0], conn) == -1)
			perror("send_fd() error");

	}

	return 0;
}